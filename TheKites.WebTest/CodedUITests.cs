﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace TheKites.WebTest
    {
    /// <summary>
    /// Summary description for CodedUITests
    /// </summary>
    [CodedUITest]
    public class CodedUITests
        {
        public CodedUITests()
            {
            }

        [TestInitialize]
        public void PrepareTestEnvironment()
            {
            this.UIMap.OpenBrowser();
            this.UIMap.NavigateToHomePageAndClosePopup();
            }

        [TestMethod]
        public void AboutUsPageIsDisplayedWhenAboutUsLinkIsClicked()
            {
            this.UIMap.ClickAboutUsLink();
            this.UIMap.AssertThatTheAboutUsPageIsDisplayed();
            }

        [TestMethod]
        public void BatchTimingPopupIsDisplayedWhenBatchTimingIsClicked()
            {
            this.UIMap.ClickBatchTiming();
            this.UIMap.AssertThatTheBatchTimingPopupIsDisplayed();
            }

        [TestMethod]
        public void RegistrationPopupIsDisplayedWhenRegisterLinkIsClicked()
            {
            this.UIMap.ClickRegisterLink();
            this.UIMap.AssertThatTheRegisterPopupIsDisplayed();
            }

        [TestMethod]
        public void MissionPageIsdisplayedWhenMissionLinkIsClicked()
            {
            this.UIMap.ClickMissionAndVisionLink();
            this.UIMap.AssertThatTheMissionVisionPageIsDisplayed();
            }

        [TestMethod]
        public void SeleniumTrainingPageIsDisplayedWhenSeleniumIsClickedOnTheIndustrialTrainingDropDownMenu()
            {
            this.UIMap.ClickSeleniumDropDown();
            this.UIMap.AssertThatTheSeleniumTrainingPageIsDisplayed();
            }

        [TestMethod]
        public void FormSubmissionFailsWhenBadCaptchaCodeEntered()
            {
            this.UIMap.NavigateToContactUsPage();
            this.UIMap.FillInContactUsFormWithBadCaptcha();
            this.UIMap.AssertThatThePageIsRedisplayedWithAValidationError();

            }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
            {
            get
                {
                return testContextInstance;
                }
            set
                {
                testContextInstance = value;
                }
            }
        private TestContext testContextInstance;

        public UIMap UIMap
            {
            get
                {
                if ((this.map == null))
                    {
                    this.map = new UIMap();
                    }

                return this.map;
                }
            }

        private UIMap map;
        }
    }
