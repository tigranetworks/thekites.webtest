﻿import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class TestCases {
    WebDriver driver;
	@BeforeMethod 
	public void InitiateBrowser() throws InterruptedException 
        {
            // Start the browser and closes the welcome popup
            System.setProperty("webdriver.chrome.driver", "tools/chromedriver.exe");
            driver= new ChromeDriver();
            driver.get("http://thekites.in");
            Thread.sleep(3000);
            driver.findElement(By.xpath("//button[@class='close' and @type='button']")).click();
            Thread.sleep(3000);
        }
	@Test
	public void BatchTiming() throws InterruptedException
        {
            //Click on Batch timing and check if popup has opened
            driver.findElement(By.xpath("//a[@id='bt']")).click();
            Thread.sleep(5000);
            Assert.assertEquals(driver.findElement(By.xpath("//button[@class='close' and @type='button']/following-sibling::h4[contains(text(),'Batch')]")).getText(),"Batch Timing");
            //Assert.assertTrue(driver.findElement(By.xpath("//button[@class='close' and @type='button']/following-sibling::h4[contains(text(),'Batch')]")));
		
            WebElement element=  driver.findElement(By.xpath("//div[@id='myModal']/div[@class='modal-dialog']//div[@class='modal-header']/button[@class='close' and @type='button']"));
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();",element);
            driver.close();
        }
	@Test
	public void AboutUs() throws InterruptedException
        {
            //Click on About US and check if user gets to the correct page
            driver.findElement(By.xpath("//a[@href='about']")).click();
            Assert.assertEquals(driver.findElement(By.xpath("//section[@class='intro']/div[@class='container ']/h1[contains(text(),'About')]")).getText(),"About The Kites");
            driver.close();
		
        }
	@Test
	public void Training() throws InterruptedException
        {
            //Click on Register for 6weeks training and check if popup opens
            driver.findElement(By.xpath("//a[text()='Register For 6 Weeks/6 Months Training']")).click();
            Thread.sleep(5000);
            //System.out.print(driver.findElement(By.xpath("//div[@id='modal-register']//h3[@class='nm text-center']")).getText());
            Assert.assertEquals(driver.findElement(By.xpath("//div[@id='modal-register']//h3[@class='nm text-center']")).getText(),"Looking for Training?");
            driver.close();	
        }
	@Test
	public void Mission() throws InterruptedException
        {
            //Click on Mission & Vision and check if user is redirected to correct page
            driver.findElement(By.xpath("//a[@href='mission_vision' and contains(text(),'Mission & Vision')]")).click();
            Thread.sleep(5000);
            //System.out.print(driver.findElement(By.xpath("//div[@id='choose']//h1[@class='text-center']/span")).getText());
            Assert.assertEquals(driver.findElement(By.xpath("//div[@id='choose']//h1[@class='text-center']/span")).getText(),"Mission & Vision");
            driver.close();	
		
        }
	@Test
	public void IndustrialTraining() throws InterruptedException
        {
            //Hover on Industrial training and click on selenium from dropdown
            Actions action = new Actions(driver);
            WebElement we = driver.findElement(By.xpath("//a[@href='http://thekites.in/industrial_training_scholarship_and_stipend_programme.php' and contains(text(),'Industrial Training')]"));
            action.moveToElement(we).build().perform();
            Thread.sleep(3000);
            driver.findElement(By.xpath("//a[@href='industrial_training_in_selenium_in_jalandhar' and contains(text(),'Selenium')]")).click();
            Thread.sleep(3000);
            //System.out.print(driver.findElement(By.xpath("//div[@id='choose']//h1[@class='text-center']//span")).getText());
            Assert.assertEquals(driver.findElement(By.xpath("//div[@id='choose']//h1[@class='text-center']//span")).getText(),"Selenium Training Jalandhar");
            driver.close();	
        }
    }

